FROM node:21.7.1-alpine3.19 AS dev
WORKDIR /app/
COPY package*.json ./
RUN npm install
COPY ./src ./src
CMD [ "npm", "run", "dev" ]

FROM dev AS builder
WORKDIR /app/
RUN npm ci --only=production && \ 
    npm audit fix --force && \
    npm run prettify && \
    rm -fr node_modules
FROM node:21.7.1-alpine3.19 AS runner
EXPOSE 3000
WORKDIR /app/
COPY --from=builder /app/package*.json /app/
RUN npm ci --only=production && \
    rm -rf package*.json
COPY --from=builder /app/src /app/
CMD ["node", "./index.js"]

